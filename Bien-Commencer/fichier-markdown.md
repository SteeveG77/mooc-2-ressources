# Partie I

## Sous - Partie I : texte

Une phrase sans rien
* Une phrase en italique *
** Une phrase en gras **
Un lien vers [Moo-fun](https://lms.fun-mooc.fr)
Une ligne de '''Python code

## Sous - Partie II : listes

Liste à puce

- item
    - sous-item
    - sous-item
- item 
- item

Liste numérotée

1. item
2. item
3. item

## Sous - Partie III : code

''' Python
# Extrait de code
